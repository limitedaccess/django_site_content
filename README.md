# README #

This is supposed to be an app to handle site specified content for different websites sharing the same code base.

### What is this repository for? ###

* Handle Site Specified content for different sites using same django code base
* version 0.1

### How to use ###

* Set up the content for different sites in the admin
* use the template tag in the template to render the content accordingly