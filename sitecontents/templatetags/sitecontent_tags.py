from django import template
from django.contrib.sites.models import Site

from sitecontents.models import SiteContent

register = template.Library()


def dequote(args):
    return "".join((arg[1:-1] if arg[0] == arg[-1] and arg[0] in ('"', "'") else arg) for arg in args)


@register.tag
def load_content(parser, token):
    """tag to load the content of for site"""
    content_length = len(token.split_contents())
    default_content = ""
    if content_length == 4:
        try:
            tag_name, content_name, default, default_content = token.split_contents()
        except ValueError:
            raise template.TemplateSyntaxError(" %r tag require default value" % token.contents.split()[0])
    else:
        try:
            tag_name, content_name = token.split_contents()
        except ValueError:
            raise template.TemplateSyntaxError(" %r tag require at least one argument" % token.contents.split()[0])
    # call the renderer
    return LoadContentNode(content_name, default_content)


class LoadContentNode(template.Node):
    def __init__(self, content_name, default=""):
        """docstring for __init__"""
        self.content_name = dequote(content_name)
        self.default = dequote(default)

    def render(self, context):
        """render this node"""
        try:
            site_content = SiteContent.objects.get(content_title__name=self.content_name, site=Site.objects.get_current())
            content = site_content.content
        except:
            content = self.default
        return content

