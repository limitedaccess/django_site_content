from django.db import models
from django.contrib.sites.models import Site
from django.utils.translation import ugettext as _


# Create your models here.
class ContentTitle(models.Model):
    name = models.CharField(max_length=128, verbose_name=_('name'))

    def __unicode__(self):
        return self.name


class SiteContent(models.Model):
    content_title = models.ForeignKey(ContentTitle)
    content = models.TextField()
    site = models.ForeignKey(Site, null=True, blank=True)

