from django.contrib import admin

from .models import ContentTitle, SiteContent


class SiteContentAdmin(admin.ModelAdmin):
    list_display = ["content_title", "content", "site"]


class ContentTitleAdmin(admin.ModelAdmin):
    list_display = ["name"]


admin.site.register(ContentTitle, ContentTitleAdmin)
admin.site.register(SiteContent, SiteContentAdmin)
